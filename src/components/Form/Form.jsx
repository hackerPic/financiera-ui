// @flow
import React from "react";
import PropTypes from "prop-types";
import moment from "moment";
import TextField from "material-ui/TextField";
import FileIcon from "material-ui/svg-icons/file/file-upload";
import FloatingActionButton from "material-ui/FloatingActionButton";
import Checkbox from "material-ui/Checkbox";
import { RadioButton, RadioButtonGroup } from "material-ui/RadioButton";
import DatePicker from "material-ui/DatePicker";
import areIntlLocalesSupported from "intl-locales-supported";
import { getDateObject } from "../util/formats";
import { EE } from "../util/emitter";
import event from "../util/events";
import { getTextError } from "./FormUtil";
import Select from "./Select/Select";

let DateTimeFormat,
    onChangeValueEvent = data => {
        const { value, name, form, epic, module } = data;

        EE.emit(event.FORM_CHANGE_VALUE, { value, name, form, epic, module });
    };

if (areIntlLocalesSupported(["es", "es-MX"])) {
    DateTimeFormat = global.Intl.DateTimeFormat;
}

const Form = props => {
    let {
            styleContainer,
            inputs,
            onChangeInputs,
            onKeyPress,
            onFocus,
            onBlur,
            formName,
            epic,
            module,
            maxHeight,
            underlineStyle,
            underlineFocusStyle
        } = props,
        handleOnChange = (name, value) => {
            if (typeof onChangeInputs === "undefined") {
                onChangeValueEvent({
                    value: value,
                    name: name,
                    form: formName,
                    epic,
                    module
                });
            } else {
                onChangeInputs(value, name);
            }
        },
        getForm = () => {
            return (
                inputs &&
                inputs.length &&
                inputs.map((property, i) => {
                    let {
                            value = "",
                            options = [],
                            type = "textfield",
                            label,
                            mini,
                            multiLine = false,
                            rows,
                            unix,
                            disabled = false,
                            hidden = false,
                            multiple = false,
                            accept = [],
                            min,
                            max,
                            name,
                            autoOk
                        } = property,
                        errorText = getTextError(property),
                        inputId = name;

                    if (hidden) {
                        return;
                    }

                    switch (type) {
                        case "checkbox":
                            if (typeof value !== "boolean") {
                                value = false;
                            }

                            return (
                                <Checkbox
                                    key={`form-checkbox-${i}`}
                                    label={label}
                                    name={name}
                                    onCheck={(event, isInputChecked) => {
                                        handleOnChange(inputId, isInputChecked);
                                    }}
                                    style={{ marginTop: "20px" }}
                                    checked={value}
                                    labelPosition="left"
                                />
                            );

                        case "select":
                            return (
                                <Select
                                    maxHeight={maxHeight}
                                    key={`form-select-${name}-${i}`}
                                    {...property}
                                    onChange={handleOnChange}
                                />
                            );
                        case "date":
                            value = getDateObject(value, unix);

                            let maxDate = null,
                                minDate = null;

                            if (min) {
                                minDate = moment(min, "DD/MM/YYYY").toDate();
                            }

                            if (max) {
                                maxDate = moment(max, "DD/MM/YYYY").toDate();
                            }

                            return (
                                <DatePicker
                                    key={`form-date-picker-${i}`}
                                    hintText={label}
                                    value={value}
                                    minDate={minDate}
                                    maxDate={maxDate}
                                    floatingLabelText={label}
                                    fullWidth={true}
                                    disabled={disabled}
                                    locale="es-MX"
                                    errorText={errorText}
                                    DateTimeFormat={DateTimeFormat}
                                    container="inline"
                                    mode="landscape"
                                    autoOk={autoOk}
                                    onChange={(event, newDate) => {
                                        handleOnChange(inputId, newDate);
                                    }}
                                />
                            );

                        case "file":
                            return (
                                <FloatingActionButton
                                    key={`form-file-${i}`}
                                    containerElement="label"
                                    secondary={true}
                                    mini={mini}
                                    disabled={disabled}
                                    className="mn-float-a-btn"
                                    label={label}
                                >
                                    <FileIcon />
                                    {!disabled && (
                                        <input
                                            multiple={multiple}
                                            accept={accept.join()}
                                            type="file"
                                            style={{ display: "none" }}
                                            onClick={e => {
                                                e.target.value = null;
                                            }}
                                            onChange={e => {
                                                const formData = new FormData();
                                                formData.append(
                                                    name,
                                                    e.target.files[0]
                                                );
                                                handleOnChange(
                                                    inputId,
                                                    formData
                                                );
                                            }}
                                        />
                                    )}
                                </FloatingActionButton>
                            );

                        case "radioButton":
                            return (
                                <RadioButtonGroup
                                    key={`form-radio-group-${i}`}
                                    name="shipSpeed"
                                    style={{ display: "flex" }}
                                    onChange={(event, selected) => {
                                        handleOnChange(inputId, selected);
                                    }}
                                    valueSelected={value}
                                >
                                    {options.map((option, index) => {
                                        return (
                                            <RadioButton
                                                key={`form-radio-item-${index}`}
                                                value={option.value}
                                                style={{
                                                    display: "inline-block",
                                                    minWidth: "210px"
                                                }}
                                                disabled={option.disabled}
                                                label={option.label}
                                            />
                                        );
                                    })}
                                </RadioButtonGroup>
                            );

                        case "textfield":
                        default:
                            return (
                                <TextField
                                    key={`form-textfield-${i}`}
                                    hintText={label}
                                    errorText={errorText}
                                    disabled={disabled && disabled}
                                    floatingLabelText={label}
                                    value={
                                        typeof value !== "undefined" && value
                                    }
                                    rows={rows}
                                    multiLine={multiLine}
                                    onChange={(event, newValue) => {
                                        handleOnChange(inputId, newValue);
                                    }}
                                    onKeyPress={onKeyPress}
                                    onFocus={onFocus}
                                    onBlur={onBlur}
                                    name={name}
                                    fullWidth={true}
                                    underlineStyle={underlineStyle}
                                    underlineFocusStyle={underlineFocusStyle}
                                />
                            );
                    }
                })
            );
        };

    return <div style={styleContainer}>{getForm()}</div>;
};

Form.propTypes = {
    onChangeInputs: PropTypes.func,
    onKeyPress: PropTypes.func,
    onFocus: PropTypes.func,
    onBlur: PropTypes.func,
    formName: PropTypes.string,
    epic: PropTypes.string,
    module: PropTypes.string,
    styleContainer: PropTypes.object,
    inputs: PropTypes.array.isRequired,
    customError: PropTypes.string,
    passedValidation: PropTypes.bool,
    maxHeight: PropTypes.number
};

export default Form;
