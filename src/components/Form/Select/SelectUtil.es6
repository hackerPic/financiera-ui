//@flow

// TODO, temporal
export const getTextError = (data: {
    autoComplete?: boolean,
    multiple: boolean,
    required: boolean,
    value: {} | [],
    errorText: string
}) => {
    const { required, value, errorText, multiple, autoComplete } = data;

    if (required) {
        if (!autoComplete && multiple) {
            if (!Array.isArray(value)) {
                return errorText;
            }

            if (value.length === 0) {
                return errorText;
            }

            return;
        }

        if (
            typeof value === "undefined" ||
            value === null ||
            typeof value.value === "undefined" ||
            value.value === null ||
            value.value === ""
        ) {
            return errorText;
        }
    }
};

export const isCheckedValue = (
    value: { value: string } | [],
    multiple: boolean,
    item: { value: string }
): boolean => {
    if (multiple) {
        if (!Array.isArray(value)) {
            value = [];
        }
        return value.findIndex(a => item.value === a.value) !== -1;
    } else {
        return item.value === value.value;
    }
};

export const getDefaultValue = (multiple: boolean): null | {} => {
    if (multiple) {
        return null;
    } else {
        return {};
    }
};

export const getSelectedValue = (
    multiple: boolean,
    value: {} | []
): [] | {} => {
    if (multiple) {
        if (!Array.isArray(value)) {
            return [];
        }
        return value;
    } else {
        return value.value;
    }
};

export const getItemValue = (multiple: boolean, item: {} | []): [] | {} => {
    if (multiple) {
        return item;
    } else {
        return item.value;
    }
};
