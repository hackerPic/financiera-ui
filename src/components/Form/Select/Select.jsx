// @flow
import React from "react";
import PropTypes from "prop-types";
import SelectField from "material-ui/SelectField";
import TextField from "material-ui/TextField";
import MenuItem from "material-ui/MenuItem";
import AutoComplete from "material-ui/AutoComplete";
import {
    getDefaultValue,
    getItemValue,
    getSelectedValue,
    getTextError,
    isCheckedValue
} from "./SelectUtil";

const Select = (props: {
    label: string,
    disabled: boolean,
    disabledOption: boolean,
    name: string,
    errorText: string,
    value: Object,
    multiple: boolean,
    collection: [],
    required: boolean,
    autoComplete: boolean,
    maxHeight: number,
    onChange: Function,
    floatingLabelFixed: boolean
}) => {
    const {
        label,
        disabled,
        disabledOption,
        name,
        errorText,
        multiple,
        collection,
        required,
        autoComplete,
        maxHeight,
        floatingLabelFixed,
        onChange
    } = props;
    let { value } = props;

    const onChangeSelectField = (event, index, newValue) => {
        let selected;

        if (multiple) {
            selected = [];
            newValue.map(nv => {
                if (nv !== null) {
                    selected.push(nv);
                }
            });
        } else {
            selected = collection[index - 1];
        }
        onChange(name, selected);
    };

    if (disabled) {
        return (
            <TextField
                hintText={label}
                disabled={disabled}
                floatingLabelFixed={floatingLabelFixed}
                floatingLabelText={label}
                name={name}
                fullWidth={true}
            />
        );
    }

    if (!autoComplete) {
        return (
            <SelectField
                floatingLabelText={label}
                value={getSelectedValue(multiple, value)}
                errorText={getTextError({
                    multiple,
                    required,
                    value,
                    errorText
                })}
                maxHeight={maxHeight}
                disabled={disabledOption}
                fullWidth={true}
                multiple={multiple}
                onChange={onChangeSelectField}
            >
                <MenuItem
                    value={getDefaultValue(multiple)}
                    primaryText="Selecciona una opción"
                />
                {collection.map((item, i) => {
                    return (
                        <MenuItem
                            key={`form-select-${name}-${i}-item`}
                            checked={isCheckedValue(value, multiple, item)}
                            value={getItemValue(multiple, item)}
                            primaryText={item.text}
                        />
                    );
                })}
            </SelectField>
        );
    }

    return (
        <AutoComplete
            name={name}
            searchText={value.text}
            floatingLabelText={label}
            filter={AutoComplete.fuzzyFilter}
            dataSource={collection}
            fullWidth={true}
            menuStyle={{ maxHeight: maxHeight, overflowY: "auto" }}
            errorText={getTextError({
                multiple,
                required,
                value,
                errorText,
                autoComplete
            })}
            openOnFocus={true}
            popoverProps={{
                style: {
                    overflowY: "auto"
                }
            }}
            onUpdateInput={searchText => {
                if (searchText.length > 0) {
                    return;
                }
                onChange(name, { text: "" });
            }}
            onNewRequest={selected => {
                onChange(name, selected);
            }}
        />
    );
};

Select.propTypes = {
    onChange: PropTypes.func.isRequired
};

Select.defaultProps = {
    value: {},
    collection: [],
    autoComplete: true,
    disabled: false,
    multiple: false,
    required: true,
    errorText: "Selecciona una opción *"
};

export default Select;
