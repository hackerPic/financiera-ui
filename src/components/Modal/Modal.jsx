import React from "react";
import PropTypes from "prop-types";
import Dialog from "material-ui/Dialog";
import FlatButton from "material-ui/FlatButton";
import Spinner from "../Spinner/Spinner";

/**
 * @param {{}} props : dfnodsfnkl
 * @return {XML} : knoofdnkl
 * @constructor
 */
const Modal = props => {
    const {
            title,
            open,
            enableEsc = true,
            showSpinner = false,
            disabledOk = false,
            labelOk = "Aceptar",
            labelCancel = "Cancelar",
            onRequestClose,
            onTouchTapOk,
            onTouchTapCancel,
            contentStyle = {
                width: "95%",
                maxWidth: "none"
            }
        } = props,
        actions = [];
    if (onTouchTapCancel) {
        actions.push(
            onTouchTapCancel && (
                <FlatButton
                    label={labelCancel}
                    primary={true}
                    onTouchTap={onTouchTapCancel}
                />
            )
        );
    }

    if (onTouchTapOk) {
        actions.push(
            onTouchTapOk && (
                <FlatButton
                    label={labelOk}
                    disabled={disabledOk}
                    primary={true}
                    onTouchTap={onTouchTapOk}
                />
            )
        );
    }

    return (
        <Dialog
            title={title}
            actions={actions}
            modal={enableEsc}
            autoScrollBodyContent={true}
            contentStyle={contentStyle}
            open={open}
            onRequestClose={onRequestClose}
        >
            {props.children}
            {showSpinner && <Spinner visible={showSpinner} />}
        </Dialog>
    );
};

Modal.propTypes = {
    title: PropTypes.string.isRequired,
    labelOk: PropTypes.string,
    labelCancel: PropTypes.string,
    open: PropTypes.bool.isRequired,
    onRequestClose: PropTypes.func,
    onTouchTapOk: PropTypes.func,
    onTouchTapCancel: PropTypes.func,
    showSpinner: PropTypes.bool,
    disabledOk: PropTypes.bool,
    enableEsc: PropTypes.bool,
    contentStyle: PropTypes.object,
    children: PropTypes.node
};

export default Modal;
