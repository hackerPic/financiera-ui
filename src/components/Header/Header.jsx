import React from "react";
import PropTypes from "prop-types";
import AppBar from "material-ui/AppBar";

const Header = props => {
    const {
        title,
        onLeftIconButtonTouchTap,
        iconElementRight,
        zDepth,
        showMenuIconButton
    } = props;

    return (
        <AppBar
            title={title}
            zDepth={zDepth}
            style={{ position: "fixed" }}
            onLeftIconButtonTouchTap={onLeftIconButtonTouchTap}
            iconElementRight={iconElementRight}
            showMenuIconButton={showMenuIconButton}
        />
    );
};

Header.propTypes = {
    onLeftIconButtonTouchTap: PropTypes.func,
    title: PropTypes.node.isRequired,
    iconElementRight: PropTypes.element,
    zDepth: PropTypes.number,
    showMenuIconButton: PropTypes.bool
};

export default Header;
