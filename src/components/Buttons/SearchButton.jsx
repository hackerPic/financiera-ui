import React from "react";
import PropTypes from "prop-types";

import SearchIcon from "material-ui/svg-icons/action/search";
import RaisedButton from "material-ui/RaisedButton";

const SearchButton = props => {
    const { label, onTouchTap } = props;

    return (
        <RaisedButton
            label={label}
            secondary={true}
            icon={<SearchIcon />}
            onTouchTap={onTouchTap}
        />
    );
};

SearchButton.propTypes = {
    label: PropTypes.string.isRequired,
    onTouchTap: PropTypes.func
};

export default SearchButton;
