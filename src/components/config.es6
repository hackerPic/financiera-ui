export default {
    palette: {
        primary1Color: "#1a51ae",
        accent1Color: "#FF5722"
    },
    oauth: {
        googleClientId:
            "690992039442-7i1abco1ppl0t2b2qa40dc3c5nfe2m9e.apps.googleusercontent.com",
        baseUrlDevelop: "http://35.184.246.116:8181",
        baseUrlProduction: "http://130.211.223.112:8182"
    }
};
