// @flow
import {
    getCurrencyFormat,
    getDateFormat,
    getLinkFormat,
    getPercentageFormat,
    getRenderBoolean
} from "../util/formats";
import _ from "underscore";
import update from "immutability-helper";

export const getLimitPage = (
    lengthNextData: number,
    rowSize: number
): number => {
    let limitPage = parseInt(lengthNextData / rowSize, 10);

    if (lengthNextData % 10 !== 0) {
        limitPage += 1;
    }
    return limitPage;
};

export const getRowsWithCurrentPage = (
    rows: [],
    currentPage: number,
    rowSize: number
): [] => {
    let indexFinal = rowSize,
        indexInitial = 0;

    if (currentPage === 1) {
        indexInitial = 0;
    } else {
        indexInitial = (currentPage - 1) * rowSize;
        indexFinal = currentPage * rowSize;
    }

    return rows.filter((row, index) => {
        return index >= indexInitial && index < indexFinal;
    });
};

export const getRowsWithFilterText = (
    rows: [],
    headers: Array<{ key: string, sortable: string }>,
    filterText: string
): [] => {
    return rows.filter(row => {
        for (let i = 0; i < headers.length; i += 1) {
            const { key, sortable } = headers[i];

            if (row[key] && sortable) {
                let stringValue = row[key].toString();
                stringValue = stringValue.toLowerCase();

                if (stringValue.includes(filterText)) {
                    return true;
                }
            }
        }

        return false;
    });
};

export const getRowsWithFormat = (rows: [], headers: []): [] => {
    return rows.map(row => {
        for (let i = 0; i < headers.length; i += 1) {
            const {
                key,
                type,
                labelBtn,
                format,
                unix,
                renderFalseAs = getRenderBoolean(),
                renderTrueAs = getRenderBoolean(true)
            } = headers[i];

            if (type === "date" && row[key]) {
                row = update(row, {
                    [key]: {
                        $set: getDateFormat({
                            format: format,
                            isUnix: unix,
                            value: row[key]
                        })
                    }
                });
            }

            if (type === "boolean") {
                row = update(row, {
                    [key]: {
                        $set: row[key] ? renderTrueAs : renderFalseAs
                    }
                });
            }

            if (type === "currency") {
                row = update(row, {
                    [key]: {
                        $set: getCurrencyFormat(row[key])
                    }
                });
            }

            if (type === "link") {
                row = update(row, {
                    [key]: {
                        $set: getLinkFormat(row[key], labelBtn)
                    }
                });
            }

            if (type === "percentage") {
                row = update(row, {
                    [key]: {
                        $set: getPercentageFormat(row[key])
                    }
                });
            }
        }

        return row;
    });
};

export const getRowsWithSort = (
    sortName: string,
    sortType: string,
    rows: []
): [] => {
    let rowsSorted = _.sortBy(rows, sortName);

    if (sortType === "desc") {
        rowsSorted = rowsSorted.reverse();
    }

    return rowsSorted;
};

export const getSelectedByAttr = (rows: [], attrSelectable: string): [] => {
    let selected = [];

    rows.map((row, index) => {
        if (row[attrSelectable]) {
            selected.push(index);
        }
    });

    return selected;
};

export const getSelectedRowsOnDT = (rows: [], realSelections: []): [] => {
    let selected = [];

    rows.map((row, index) => {
        realSelections.map(rs => {
            if (row.index === rs) {
                selected.push(index);
            }
        });
    });

    return selected;
};

export const pushElement = (rows: [], data: []): [] => {
    if (data.length > 0 && !rows.find(row => row === data[data.length - 1])) {
        rows = [...[data[data.length - 1]], ...rows];
        rows.pop();
    }

    return rows;
};

export const getRowsWithGroupBy = (data: { rows: [], columns: {} }): [] => {
    const { rows = [], columns = {} } = data;

    let res = [...rows];

    for (const name in columns) {
        if (columns.hasOwnProperty(name)) {
            res = res.filter(item => {
                let currentValue = item[name];
                if (
                    currentValue === null ||
                    typeof currentValue === "undefined"
                ) {
                    currentValue = "SIN VALOR";
                }
                currentValue = currentValue.toString();
                currentValue = currentValue.trim();
                currentValue = currentValue.toLowerCase();

                if (currentValue.length === 0) {
                    currentValue = "sin valor";
                }
                currentValue = currentValue.toLowerCase();

                const option = columns[name];

                if (option.selected.length === 0) {
                    return true;
                }

                for (let i = 0; i < option.selected.length; i += 1) {
                    let valueSelected = option.selected[i];
                    valueSelected = valueSelected.toString();
                    valueSelected = valueSelected.trim();
                    valueSelected = valueSelected.toLowerCase();

                    if (valueSelected === currentValue) {
                        return true;
                    }
                }
                return false;
            });
        }
    }

    return res;
};

export const removeNotExist = (
    rows: [],
    realSelections: [],
    selectionOnPage: [],
    rowSize: number
): [] => {
    let indexNotExist,
        limit = 0;

    if (realSelections.length > 0) {
        if (rows.length < rowSize) {
            limit = rows.length;
        } else {
            limit = rowSize;
        }

        if (selectionOnPage !== "all") {
            for (let i = 0; i < limit; i += 1) {
                if (typeof selectionOnPage.find(s => i === s) === "undefined") {
                    indexNotExist = realSelections.indexOf(rows[i].index);

                    if (indexNotExist > -1) {
                        realSelections.splice(indexNotExist, 1);
                    }
                }
            }
        }
    }

    return realSelections;
};

export const buildObjectOrder = (obj): {} => {
    const { data, distinct, type, a } = obj;
    let format,
        values = [];

    if (typeof distinct.columns[a] === "undefined") {
        data.map(item => {
            let v = item[a];
            if (v !== null) {
                v = v.toString();
                v = v.trim();
                if (v.length > 0) {
                }
                values.push(v);
            }
        });

        if (values.length === 0) {
            return distinct;
        }

        if (typeof type !== "undefined") {
            format = type;
        } else {
            format = "";
        }

        distinct.columns[a] = {
            collection: [...new Set(values)],
            selected: [],
            format
        };

        if (format === "boolean") {
            distinct.columns[a].collection = distinct.columns[a].collection.map(
                v => v === "true"
            );
        }
    }

    return distinct;
};
