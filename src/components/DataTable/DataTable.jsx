// @flow
import React, { Component } from "react";
import PropTypes from "prop-types";
import DataTables from "material-ui-datatables";
import { Card } from "material-ui/Card";
import IconButton from "material-ui/IconButton";
import DownloadIcon from "material-ui/svg-icons/file/file-download";

import {
    getLimitPage,
    getRowsWithCurrentPage,
    getRowsWithFilterText,
    getRowsWithFormat,
    getRowsWithGroupBy,
    getRowsWithSort,
    getSelectedByAttr,
    getSelectedRowsOnDT,
    pushElement,
    removeNotExist
} from "./dataTableUtil";
import { getCSV } from "../util/files";
import SortModal from "./SortModal";

/**
 * Utiliza maquetación de material-ui-datatables y agrega features
 * como paginación, filtro, exportar csv y ordenamiento.
 */
export default class DataTable extends Component {
    rows: [];
    oneElementAdded: boolean;
    applyGroupBy: boolean;
    state: {
        realSelections: [],
        rowSize: number,
        showModal: boolean,
        currentPage: number,
        sortName: string,
        sortType: string,
        currentPage: number,
        limitPage: number,
        filterText: string,
        distinct: {
            columns: {}
        }
    };

    static propTypes = {
        onRowSelection: PropTypes.func,
        selectable: PropTypes.bool,
        selectableManually: PropTypes.bool,
        resetSelected: PropTypes.bool,
        attrSelectable: PropTypes.string,
        card: PropTypes.bool,
        scrollbar: PropTypes.bool,
        exportCSV: PropTypes.bool,
        rowSize: PropTypes.number,
        showHeaderToolbar: PropTypes.bool,
        enableSelectAll: PropTypes.bool,
        pagination: PropTypes.bool,
        showCheckboxes: PropTypes.bool,
        showFooterToolbar: PropTypes.bool,
        multiSelectable: PropTypes.bool,
        title: PropTypes.string,
        titleFileCSV: PropTypes.string,
        data: PropTypes.array.isRequired,
        tooltipBtnCSV: PropTypes.string,
        rowSizeList: PropTypes.arrayOf(PropTypes.number),
        headers: PropTypes.array.isRequired,
        deselectOnClickaway: PropTypes.bool
    };
    static defaultProps = {
        data: [],
        selectable: false,
        scrollbar: true,
        pagination: true,
        exportCSV: true,
        titleFileCSV: "reporte",
        tooltipBtnCSV: "Descargar CSV",
        rowSize: 5,
        rowSizeList: [5, 10, 15, 20],
        card: true,
        showCheckboxes: false,
        resetSelected: false,
        selectableManually: false,
        showHeaderToolbar: true,
        showFooterToolbar: true,
        enableSelectAll: false,
        multiSelectable: false,
        deselectOnClickaway: true
    };

    constructor(props: {}, context: {}) {
        super(props, context);
        this.handleChangeValueFilter = ::this.handleChangeValueFilter;
        this.handlePrevPage = ::this.handlePrevPage;
        this.handleNextPage = ::this.handleNextPage;
        this.handleRowSelect = ::this.handleRowSelect;
        this.handleSortOrderChange = ::this.handleSortOrderChange;
        this.handleChangeRowSize = ::this.handleChangeRowSize;
        this.handleModalClose = ::this.handleModalClose;
        this.handleFilter = ::this.handleFilter;
        this.handleToggled = ::this.handleToggled;
        this.applyGroupBy = false;
        this.oneElementAdded = false;
        this.rows = [];

        let { rowSizeList } = this.props;
        let size = rowSizeList.find(i => i === this.props.rowSize);

        if (typeof size === "undefined") {
            size = rowSizeList[0];
        }

        this.state = {
            distinct: {
                columns: {}
            },
            showModal: false,
            currentPage: 1,
            rowSize: size,
            filterText: "",
            sortName: "",
            sortType: "",
            limitPage: getLimitPage(this.props.data.length, size),
            realSelections: this.getInitialSelected()
        };
    }

    componentWillReceiveProps = (nextProps: { data: [] }) => {
        const { data, resetSelected } = nextProps,
            lengthCurrentData = this.props.data.length,
            lengthNextData = data.length,
            { rowSize } = this.state;

        if (resetSelected !== this.props.resetSelected) {
            this.setState({
                realSelections: []
            });
        }

        if (lengthNextData !== lengthCurrentData) {
            let page;

            switch (lengthNextData - lengthCurrentData) {
                case 1:
                    page = this.state.currentPage;
                    this.oneElementAdded = true;
                    break;

                case -1:
                    page = this.state.currentPage;
                    this.oneElementAdded = true;
                    break;

                default:
                    page = 1;
                    this.oneElementAdded = false;
                    break;
            }

            this.setState({
                limitPage: getLimitPage(data.length, rowSize),
                currentPage: page,
                filterText: "",
                realSelections: this.getInitialSelected()
            });
        }
    };

    getInitialSelected = () => {
        const { selectableManually, data, attrSelectable } = this.props;

        let selections = [];

        if (selectableManually) {
            selections = getSelectedByAttr(data, attrSelectable);
        }

        return selections;
    };

    handleModalClose = () => {
        this.applyGroupBy = false;
        let distinct = {
            columns: {}
        };
        this.setState({ showModal: false, distinct });
    };

    handleFilter = () => {
        this.applyGroupBy = true;
        this.setState({ showModal: false });
    };

    handleNextPage = () => {
        const { currentPage, limitPage } = this.state;

        if (currentPage + 1 > limitPage) {
            return;
        }
        this.oneElementAdded = false;
        this.setState({ currentPage: currentPage + 1 });
    };

    handlePrevPage = () => {
        const { currentPage } = this.state;

        if (currentPage - 1 <= 0) {
            return;
        }
        this.oneElementAdded = false;
        this.setState({ currentPage: currentPage - 1 });
    };

    handleChangeValueFilter = (value: string) => {
        this.oneElementAdded = false;
        this.setState({
            currentPage: 1,
            filterText: value.trim().toLowerCase()
        });
    };

    handleChangeRowSize = (index: number) => {
        const { currentPage } = this.state,
            { data, rowSizeList } = this.props;

        let limit = getLimitPage(data.length, rowSizeList[index]),
            obj = {
                rowSize: rowSizeList[index],
                limitPage: limit,
                currentPage: currentPage
            };

        if (currentPage > limit) {
            obj.currentPage = limit;
        }

        this.setState(obj);
    };

    handleSortOrderChange = (a: string, b: string) => {
        const { headers, data } = this.props;
        let { distinct } = this.state;
        let { type } = headers.find(h => h.key === a);
        let format,
            values = [];

        if (typeof distinct.columns[a] === "undefined") {
            data.map(item => {
                let v = item[a];
                if (v !== null) {
                    if (typeof v === "undefined") {
                        v = "SIN VALOR";
                    }
                    v = v.toString();
                    v = v.trim();
                    if (v.length === 0) {
                        v = "SIN VALOR";
                    }
                    values.push(v);
                }
            });

            if (values.length === 0) {
                return;
            }

            if (typeof type !== "undefined") {
                format = type;
            } else {
                format = "";
            }

            distinct.columns[a] = {
                collection: [...new Set(values)],
                selected: [],
                format
            };
            if (format === "boolean") {
                distinct.columns[a].collection = distinct.columns[
                    a
                ].collection.map(v => v === "true");
            }
        }
        this.setState({
            distinct,
            currentPage: 1,
            sortName: a,
            sortType: b,
            showModal: true
        });
    };

    handleRowSelect = selection => {
        const { onRowSelection, data, selectableManually } = this.props;

        let { realSelections, rowSize } = this.state,
            responseArray = false;

        switch (selection) {
            case "all":
                data.map((d, index) => {
                    realSelections.push(index);
                });
                break;

            case "none":
                realSelections = [];
                break;

            default:
                responseArray = true;
                selection.map(item => {
                    data.map((d, index) => {
                        if (this.rows[item].index === index) {
                            realSelections.push(index);
                        }
                    });
                });
                break;
        }

        realSelections = [...new Set(realSelections)];

        realSelections = removeNotExist(
            this.rows,
            realSelections,
            selection,
            rowSize
        );
        onRowSelection(realSelections);
        this.setState({ realSelections });

        if (!selectableManually) {
            this.setState({ realSelections });
        }
    };

    handleOnCheckItem = (
        value: string,
        isInputChecked: boolean,
        sortName: string
    ) => {
        let { distinct } = this.state,
            index;
        if (!isInputChecked) {
            index = distinct.columns[sortName].selected.findIndex(
                i => i === value
            );
            if (index > -1) {
                distinct.columns[sortName].selected.splice(index, 1);
            }
        } else {
            distinct.columns[sortName].selected.push(value);
        }

        this.setState({ distinct });
    };

    handleToggled = (event: Object, isInputChecked: boolean) => {
        let sortType;
        if (isInputChecked) {
            sortType = "asc";
        } else {
            sortType = "desc";
        }

        this.setState({ sortType });
    };

    getTable = () => {
        const {
                title,
                selectable,
                headers,
                data,
                attrSelectable,
                showCheckboxes,
                selectableManually,
                pagination,
                enableSelectAll,
                titleFileCSV,
                multiSelectable,
                rowSizeList,
                tooltipBtnCSV,
                exportCSV,
                scrollbar,
                showFooterToolbar,
                showHeaderToolbar,
                deselectOnClickaway
            } = this.props,
            {
                distinct,
                currentPage,
                rowSize,
                filterText,
                sortName,
                sortType,
                realSelections
            } = this.state;

        let scrollbarCSS: {
                minHeight?: string,
                overflowX: string,
                maxHeight: string,
                overflowY: string
            } = { overflowX: "auto", overflowY: "", maxHeight: "" },
            selectedRows = [],
            total = data.length;

        if (data instanceof Array) {
            this.rows = data.map((d, index) => Object.assign({}, d, { index }));
        } else {
            this.rows = [];
        }

        if (sortName.length !== 0) {
            this.rows = getRowsWithSort(sortName, sortType, data);
        }

        if (this.applyGroupBy) {
            if (this.applyGroupBy) {
                this.rows = getRowsWithGroupBy({
                    rows: this.rows,
                    columns: distinct.columns
                });
                total = this.rows.length;
            }
        }

        if (filterText.length > 0) {
            this.rows = getRowsWithFilterText(this.rows, headers, filterText);
            total = this.rows.length;
        }

        if (pagination) {
            this.rows = getRowsWithCurrentPage(this.rows, currentPage, rowSize);
        }

        if (this.oneElementAdded) {
            this.rows = pushElement(this.rows, data);
        }

        this.rows = getRowsWithFormat(this.rows, headers);

        if (selectable) {
            if (selectableManually) {
                selectedRows = getSelectedByAttr(this.rows, attrSelectable);
            } else {
                selectedRows = getSelectedRowsOnDT(this.rows, realSelections);
            }
        }

        if (scrollbar) {
            scrollbarCSS.maxHeight = "350px";
            scrollbarCSS.overflowY = "auto";
        }

        return (
            <DataTables
                height={"auto"}
                selectable={selectable}
                showRowHover={true}
                columns={headers.map(h => {
                    let w = { width: 250 };
                    return { ...h, ...w };
                })}
                data={this.rows}
                tableBodyStyle={scrollbarCSS}
                showCheckboxes={showCheckboxes}
                enableSelectAll={enableSelectAll}
                multiSelectable={multiSelectable}
                showHeaderToolbar={showHeaderToolbar}
                onRowSelection={this.handleRowSelect}
                tableStyle={{ tableLayout: "auto" }}
                onFilterValueChange={this.handleChangeValueFilter}
                page={currentPage}
                filterHintText={"Buscar en la tabla"}
                count={total}
                selectedRows={selectedRows}
                rowSize={rowSize}
                rowSizeLabel={"Registros por página"}
                rowSizeList={rowSizeList}
                showRowSizeControls={true}
                showFooterToolbar={pagination && showFooterToolbar}
                onRowSizeChange={this.handleChangeRowSize}
                onSortOrderChange={this.handleSortOrderChange}
                onNextPageClick={this.handleNextPage}
                onPreviousPageClick={this.handlePrevPage}
                deselectOnClickaway={deselectOnClickaway}
                toolbarIconRight={
                    exportCSV && (
                        <IconButton
                            tooltip={tooltipBtnCSV}
                            onTouchTap={() => {
                                getCSV(data, headers, titleFileCSV);
                            }}
                        >
                            <DownloadIcon />
                        </IconButton>
                    )
                }
                title={title && title}
            />
        );
    };

    render = () => {
        const { card, headers } = this.props,
            { showModal, sortType, sortName, distinct } = this.state;

        let labelOrder = "";

        if (showModal) {
            labelOrder = headers.find(l => l.key === sortName);
            labelOrder = labelOrder.label;
        }

        return (
            <div>
                {card && (
                    <Card style={{ margin: 12 }} children={this.getTable()} />
                )}

                {!card && this.getTable()}

                <SortModal
                    open={showModal}
                    onClose={this.handleModalClose}
                    onFilter={this.handleFilter}
                    onChangeSortType={this.handleToggled}
                    onCheckItem={this.handleOnCheckItem}
                    sortType={sortType}
                    sortName={sortName}
                    distinct={distinct}
                    labelOrder={labelOrder}
                />
            </div>
        );
    };
}
