// @flow
import React, { Component } from "react";
import PropTypes from "prop-types";
import config from "../config";
import GoogleLogin from "react-google-login";
import AutorizacionApi from "./AutorizacionApi";
import Container from "../Container/Container";
import { saveToken } from "./util/AutorizacionUtil";
import ProgressBackground from "../RequestProgress/RequestProgress";

/**
 * Realiza las peticiones indicadas por financiera para validar un usuario en conjunto con Google oauth
 */
export default class Authorization extends Component {
    state: {
        loading: boolean
    };

    static propTypes = {
        logo: PropTypes.string,
        isSandbox: PropTypes.bool,
        googleClientId: PropTypes.string,
        baseUrlDevelop: PropTypes.string,
        baseUrlProduction: PropTypes.string,
        email: PropTypes.string,
        onSuccess: PropTypes.func.isRequired,
        onFailure: PropTypes.func.isRequired
    };

    static defaultProps = {
        isSandbox: true,
        googleClientId: config.oauth.googleClientId,
        baseUrlDevelop: config.oauth.baseUrlDevelop,
        baseUrlProduction: config.oauth.baseUrlProduction
    };

    baseUrl: string;

    constructor(props: {}, context: {}) {
        super(props, context);
        this.handleLoginFailure = ::this.handleLoginFailure;
        this.handleLoginSuccess = ::this.handleLoginSuccess;
        this.baseUrl = "";
        this.state = {
            loading: false
        };
        const { email, isSandbox } = this.props;

        if (isSandbox && typeof email === "undefined") {
            console.error("Requiere la propiedad email");
        }
    }

    handleLoginFailure = (response: {}) => {
        const { onFailure } = this.props;
        onFailure(response);
    };

    handleLoginSuccess = async (response: {
        accessToken: string,
        profileObj: { email: string }
    }) => {
        const { accessToken, profileObj } = response,
            {
                onSuccess,
                isSandbox,
                onFailure,
                email,
                baseUrlDevelop,
                baseUrlProduction
            } = this.props;

        try {
            let correo = profileObj.email,
                user;

            if (isSandbox) {
                correo = email || correo;
                this.baseUrl = baseUrlDevelop;
            } else {
                this.baseUrl = baseUrlProduction;
            }
            this.setState({ loading: true });

            user = await new AutorizacionApi().validar(correo, this.baseUrl);

            saveToken(accessToken);
            onSuccess(user);
        } catch (err) {
            onFailure(err);
        } finally {
            this.setState({ loading: false });
        }
    };

    render = () => {
        const { logo, googleClientId } = this.props,
            { loading } = this.state;

        return (
            <div className="row">
                <ProgressBackground
                    message={"Validando usuario"}
                    open={loading}
                />

                <div className="col-xs-12 col-md-4 col-md-offset-4">
                    <Container>
                        <div className="row">
                            <div className="col-xs-12 text-center">
                                {logo && <img src={logo} />}
                            </div>
                        </div>

                        <div className="row" style={{ marginTop: "50px" }}>
                            <div className="col-xs-12 text-center">
                                <GoogleLogin
                                    clientId={googleClientId}
                                    scope={
                                        "profile email https://www.googleapis.com/auth/calendar"
                                    }
                                    buttonText="Iniciar sesión con Google"
                                    discoveryDocs={[
                                        "https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest"
                                    ]}
                                    onSuccess={this.handleLoginSuccess}
                                    onFailure={this.handleLoginFailure}
                                />
                            </div>
                        </div>
                    </Container>
                </div>
            </div>
        );
    };
}
