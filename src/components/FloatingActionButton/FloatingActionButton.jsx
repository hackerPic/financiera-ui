import React from "react";
import PropTypes from "prop-types";
import FloatingActionButton from "material-ui/FloatingActionButton";
import ContentAdd from "material-ui/svg-icons/content/add";

const FloatingButton = props => {
    const { onTouchTap, mini, style } = props;

    return (
        <FloatingActionButton
            mini={mini}
            secondary={true}
            className="mn-float-a-btn"
            onTouchTap={onTouchTap}
            style={style}
        >
            <ContentAdd />
        </FloatingActionButton>
    );
};

FloatingButton.propTypes = {
    onTouchTap: PropTypes.func.isRequired,
    style: PropTypes.object,
    mini: PropTypes.bool
};

export default FloatingButton;
