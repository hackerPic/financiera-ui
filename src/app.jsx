/* @flow */
import React from "react";
import { render } from "react-dom";
import ThemeFinanciera from "./components/ThemeFinanciera/ThemeFinanciera";
import injectTapEventPlugin from "react-tap-event-plugin";
import "babel-polyfill";
import "!style-loader!css-loader!highlight.js/styles/ocean.css";
import "!style-loader!css-loader!./assets/css/main.css";
import Docs from "./docs/Docs";

injectTapEventPlugin();

render(
    <ThemeFinanciera>
        <Docs />
    </ThemeFinanciera>,
    document.getElementById("root")
);
