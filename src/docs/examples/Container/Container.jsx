// @flow
import React, { Component } from "react";
import RaisedButton from "material-ui/RaisedButton";
import { Container } from "financiera-ui";

export default class App extends Component {
    constructor(props: {}, context: {}) {
        super(props, context);
    }

    render = () => {
        const style = { margin: 10 };

        return (
            <Container title="Lista de usuarios" showSpinner={true}>
                <RaisedButton label="Default" style={style} />
                <RaisedButton label="Primary" primary={true} style={style} />
                <RaisedButton
                    label="Secondary"
                    secondary={true}
                    style={style}
                />
                <RaisedButton label="Disabled" disabled={true} style={style} />
                <br />
                <br />
                <RaisedButton label="Full width" fullWidth={true} />
            </Container>
        );
    };
}
