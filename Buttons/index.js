"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.SearchButton = undefined;

var _SearchButton2 = require("./SearchButton");

var _SearchButton3 = _interopRequireDefault(_SearchButton2);

function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : { default: obj };
}

exports.SearchButton = _SearchButton3.default;
/* eslint object-curly-spacing: 0 */
