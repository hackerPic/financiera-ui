"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.saveToken = exports.isThereATokenValid = undefined;

var _regenerator = require("babel-runtime/regenerator");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = require("babel-runtime/helpers/extends");

var _extends3 = _interopRequireDefault(_extends2);

var _asyncToGenerator2 = require("babel-runtime/helpers/asyncToGenerator");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _axios = require("axios");

var _axios2 = _interopRequireDefault(_axios);

var _AutorizacionApi = require("../AutorizacionApi");

var _AutorizacionApi2 = _interopRequireDefault(_AutorizacionApi);

var _config = require("../../config");

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : { default: obj };
}

var isThereATokenValid = (exports.isThereATokenValid = (function() {
        var _ref = (0, _asyncToGenerator3.default)(
            /*#__PURE__*/ _regenerator2.default.mark(function _callee() {
                var data =
                    arguments.length > 0 && arguments[0] !== undefined
                        ? arguments[0]
                        : { isSanbox: true };
                var email, isSanbox, response, token, userValid, instance;
                return _regenerator2.default.wrap(
                    function _callee$(_context) {
                        while (1) {
                            switch ((_context.prev = _context.next)) {
                                case 0:
                                    (email = data.email),
                                        (isSanbox = data.isSanbox);
                                    _context.prev = 1;
                                    (response = void 0),
                                        (token = sessionStorage.getItem(
                                            "token"
                                        )),
                                        (userValid = void 0);

                                    if (
                                        !(
                                            typeof token === "undefined" ||
                                            token === null
                                        )
                                    ) {
                                        _context.next = 5;
                                        break;
                                    }

                                    return _context.abrupt("return", false);

                                case 5:
                                    instance = _axios2.default.create();
                                    _context.next = 8;
                                    return instance.request({
                                        url: "/oauth2/v3/tokeninfo",
                                        method: "get",
                                        baseURL: "https://www.googleapis.com/",
                                        params: {
                                            access_token: token
                                        },
                                        timeout: 100000
                                    });

                                case 8:
                                    response = _context.sent;

                                    userValid =
                                        response.data["email_verified"] ===
                                        "true";
                                    _context.next = 12;
                                    return new _AutorizacionApi2.default().validar(
                                        email ? email : response.data.email,
                                        isSanbox
                                            ? _config2.default.oauth
                                                  .baseUrlDevelop
                                            : _config2.default.oauth
                                                  .baseUrlProduction
                                    );

                                case 12:
                                    response = _context.sent;
                                    return _context.abrupt(
                                        "return",
                                        (0, _extends3.default)(
                                            {
                                                userValid: userValid
                                            },
                                            response
                                        )
                                    );

                                case 16:
                                    _context.prev = 16;
                                    _context.t0 = _context["catch"](1);
                                    return _context.abrupt("return", {
                                        userValid: false,
                                        perfiles: []
                                    });

                                case 19:
                                case "end":
                                    return _context.stop();
                            }
                        }
                    },
                    _callee,
                    undefined,
                    [[1, 16]]
                );
            })
        );

        return function isThereATokenValid() {
            return _ref.apply(this, arguments);
        };
    })()),
    saveToken = (exports.saveToken = function saveToken(token) {
        sessionStorage.setItem("token", token);
    });
