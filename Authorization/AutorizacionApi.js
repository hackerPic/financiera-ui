"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = undefined;

var _regenerator = require("babel-runtime/regenerator");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = require("babel-runtime/helpers/extends");

var _extends3 = _interopRequireDefault(_extends2);

var _asyncToGenerator2 = require("babel-runtime/helpers/asyncToGenerator");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _classCallCheck2 = require("babel-runtime/helpers/classCallCheck");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _possibleConstructorReturn2 = require("babel-runtime/helpers/possibleConstructorReturn");

var _possibleConstructorReturn3 = _interopRequireDefault(
    _possibleConstructorReturn2
);

var _inherits2 = require("babel-runtime/helpers/inherits");

var _inherits3 = _interopRequireDefault(_inherits2);

var _Request2 = require("../util/Request");

var _Request3 = _interopRequireDefault(_Request2);

function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : { default: obj };
}

var AutorizacionApi = (function(_Request) {
    (0, _inherits3.default)(AutorizacionApi, _Request);

    function AutorizacionApi() {
        var _this2 = this;

        (0, _classCallCheck3.default)(this, AutorizacionApi);

        var _this = (0, _possibleConstructorReturn3.default)(
            this,
            (
                AutorizacionApi.__proto__ ||
                Object.getPrototypeOf(AutorizacionApi)
            ).call(this)
        );

        _this.validar = (function() {
            var _ref = (0, _asyncToGenerator3.default)(
                /*#__PURE__*/ _regenerator2.default.mark(function _callee(
                    email,
                    baseUrl
                ) {
                    var user, response;
                    return _regenerator2.default.wrap(
                        function _callee$(_context) {
                            while (1) {
                                switch ((_context.prev = _context.next)) {
                                    case 0:
                                        _context.prev = 0;
                                        user = {};
                                        _context.next = 4;
                                        return _this.fetch({
                                            baseUrl: baseUrl,
                                            url:
                                                "cxf/seguridad/autorizacion/consultarUsuarioEmail",
                                            data: {
                                                email: email
                                            }
                                        });

                                    case 4:
                                        response = _context.sent;

                                        user.idPersona = response.idPersona;
                                        user.claveUsuario =
                                            response.claveUsuario;
                                        user.email = response.email;

                                        if (!(response === null)) {
                                            _context.next = 10;
                                            break;
                                        }

                                        throw new Error(
                                            "El correo no es válido"
                                        );

                                    case 10:
                                        _context.next = 12;
                                        return _this.fetch({
                                            baseUrl: baseUrl,
                                            url:
                                                "cxf/empleados/rest/buscarActivosSirh",
                                            data: {
                                                usuario: user.claveUsuario
                                            }
                                        });

                                    case 12:
                                        response = _context.sent;

                                        user.nombre = response.nombre;
                                        user.apellidoPaterno =
                                            response.apellidoPaterno;
                                        user.apellidoMaterno =
                                            response.apellidoMaterno;
                                        user.sucursal = {};
                                        user.sucursal.clave =
                                            response.claveSucursal;
                                        user.sucursal.descripcion =
                                            response.descripcionSucursal;
                                        user.puesto = {};
                                        user.puesto.clave =
                                            response.clavePuesto;
                                        user.puesto.descripcion =
                                            response.descripcionPuesto;
                                        user.empresa = {};
                                        user.empresa.clave =
                                            response.claveEmpresa;
                                        user.empresa.descripcion =
                                            response.claveEmpresa;

                                        if (!(response.length === 0)) {
                                            _context.next = 27;
                                            break;
                                        }

                                        throw new Error(
                                            "El usuario no está activo"
                                        );

                                    case 27:
                                        _context.next = 29;
                                        return _this.fetch({
                                            baseUrl: baseUrl,
                                            url:
                                                "cxf/seguridad/autorizacion/consultarUsuarios",
                                            data: {
                                                claveUsuario: user.claveUsuario,
                                                facultades: [{}],
                                                perfiles: [{}]
                                            }
                                        });

                                    case 29:
                                        response = _context.sent;

                                        if (!(response.length === 0)) {
                                            _context.next = 32;
                                            break;
                                        }

                                        throw new Error(
                                            "No hay facultades y perfiles para este usuario"
                                        );

                                    case 32:
                                        return _context.abrupt(
                                            "return",
                                            (0, _extends3.default)({}, user, {
                                                perfiles: response[0].perfiles,
                                                facultades:
                                                    response[0].facultades
                                            })
                                        );

                                    case 35:
                                        _context.prev = 35;
                                        _context.t0 = _context["catch"](0);
                                        throw new Error(_context.t0);

                                    case 38:
                                    case "end":
                                        return _context.stop();
                                }
                            }
                        },
                        _callee,
                        _this2,
                        [[0, 35]]
                    );
                })
            );

            return function(_x, _x2) {
                return _ref.apply(this, arguments);
            };
        })();

        return _this;
    }

    return AutorizacionApi;
})(_Request3.default);

exports.default = AutorizacionApi;
