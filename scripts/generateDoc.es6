import fs from "fs";
import path from "path";
import chalk from "chalk";
import {parse} from "react-docgen";
import chokidar from "chokidar";

const paths = {
    examples: path.join(__dirname, "../src", "docs", "examples"),
    components: path.join(__dirname, "../src", "components"),
    output: path.join(__dirname, "../docs", "componentData.es6")
};

const ignore = ["docs", "assets", "examples", "util", "styles", "Menu", "Buttons", "charts"];

//TODO, DEJAR EL ==. Solo así funciona el watcher
const enableWatchMode = process.argv.slice(2) == "--watch";


const isIgnoreDir = (directory) => {
    return ignore.findIndex(d => d === directory) > -1;
};


const generate = (paths) => {
    let errors = [];

    const componentData = getDirectories(paths.components).map((componentName) => {

        try {

            return getComponentData(paths, componentName);

        } catch (error) {

            errors.push("An error occurred while attempting to generate metadata for " + componentName + ". " + error);

        }

    });

    writeFile(paths.output, "module.exports = /* eslint-disable */ " + JSON.stringify(errors.length ? errors : componentData));
};

const getComponentData = (paths, componentName) => {
    const content = readFile(path.join(paths.components, componentName, componentName + ".jsx"));
    const info = parse(content);
    return {
        name: componentName,
        description: info.description,
        props: info.props,
        code: content,
        examples: getExampleData(paths.examples, componentName)
    };
};


const getExampleData = (examplesPath, componentName) => {

    const examples = getExampleFiles(examplesPath, componentName);

    return examples.map((file) => {
            const filePath = path.join(examplesPath, componentName, file);
            const content = readFile(filePath);
            const info = parse(content);
            return {
                // By convention, component name should match the filename.
                // So remove the .js extension to get the component name.
                name: file.slice(0, -3),
                description: info.description,
                code: content
            };
        }
    );
};

const getExampleFiles = (examplesPath, componentName) => {
    let exampleFiles = [];
    try {

        exampleFiles = getFiles(path.join(examplesPath, componentName));

    } catch (error) {
        console.log(chalk.red(`No examples found for ${componentName}.`));
    }
    return exampleFiles;
};

const getDirectories = (filepath) => {
    return fs.readdirSync(filepath).filter((file) => {

        return !isIgnoreDir(file) && fs.statSync(path.join(filepath, file)).isDirectory();
    });
};

const getFiles = (filepath) => {
    return fs.readdirSync(filepath).filter((file) => {
        return fs.statSync(path.join(filepath, file)).isFile();
    });
};

const writeFile = (filepath, content) => {
    fs.writeFile(filepath, content, (err) => {
        err ? console.log(chalk.red(err)) : console.log(chalk.green("Component data saved."));
    });
};

const readFile = (filePath) => {
    return fs.readFileSync(filePath, "utf-8");
};


if (enableWatchMode) {
    // Regenerate component metadata when components or examples change.

    chokidar.watch([paths.examples, paths.components]).on("change", (event, path) => {
        generate(paths);
    });

} else {
    // Generate component metadata
    generate(paths);
}
