"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = undefined;

var _toConsumableArray2 = require("babel-runtime/helpers/toConsumableArray");

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

var _classCallCheck2 = require("babel-runtime/helpers/classCallCheck");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _possibleConstructorReturn2 = require("babel-runtime/helpers/possibleConstructorReturn");

var _possibleConstructorReturn3 = _interopRequireDefault(
    _possibleConstructorReturn2
);

var _inherits2 = require("babel-runtime/helpers/inherits");

var _inherits3 = _interopRequireDefault(_inherits2);

var _class, _temp;

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = require("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _Dialog = require("material-ui/Dialog");

var _Dialog2 = _interopRequireDefault(_Dialog);

var _RaisedButton = require("material-ui/RaisedButton");

var _RaisedButton2 = _interopRequireDefault(_RaisedButton);

var _fileUpload = require("material-ui/svg-icons/file/file-upload");

var _fileUpload2 = _interopRequireDefault(_fileUpload);

var _List = require("material-ui/List");

var _FileItem = require("./FileItem");

var _FileItem2 = _interopRequireDefault(_FileItem);

function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : { default: obj };
}

var FileManager = ((_temp = _class = (function(_Component) {
    (0, _inherits3.default)(FileManager, _Component);

    function FileManager(props, context) {
        (0, _classCallCheck3.default)(this, FileManager);

        var _this = (0, _possibleConstructorReturn3.default)(
            this,
            (FileManager.__proto__ || Object.getPrototypeOf(FileManager)).call(
                this,
                props,
                context
            )
        );

        _this.componentDidMount = function() {};

        _this.componentWillUpdate = function(nextProps, nextState) {
            var files = nextState.files,
                open = nextProps.open;

            if (files.length !== _this.state.files.length) {
                if (open && typeof _this.props.onChange !== "undefined") {
                    _this.props.onChange(files);
                }
            }
        };

        _this.componentWillUnmount = function() {};

        _this.handleClose = function() {
            _this.props.onClose(_this.state.files);
            _this.setState({ files: [] });
        };

        _this.handleOnChangeInput = function(e) {
            var files = _this.state.files;

            for (var i = 0; i < e.target.files.length; i += 1) {
                files = [e.target.files[i]].concat(
                    (0, _toConsumableArray3.default)(files)
                );
            }
            _this.setState({ files: files });
        };

        _this.handleRemoveIcon = function(index) {
            var files = [].concat(
                (0, _toConsumableArray3.default)(_this.state.files)
            );
            files.splice(index, 1);

            _this.setState({ files: files });
        };

        _this.render = function() {
            var files = _this.state.files,
                open = _this.props.open;

            var actions = [
                _react2.default.createElement(
                    _RaisedButton2.default,
                    {
                        label: "Seleccionar archivos de tu ordenador",
                        containerElement: "label",
                        secondary: true,
                        style: { marginRight: "10px" },
                        icon: _react2.default.createElement(
                            _fileUpload2.default,
                            null
                        )
                    },
                    _react2.default.createElement("input", {
                        multiple: true,
                        type: "file",
                        style: { display: "none" },
                        onClick: function onClick(e) {
                            e.target.value = null;
                        },
                        onChange: _this.handleOnChangeInput
                    })
                ),
                _react2.default.createElement(_RaisedButton2.default, {
                    label: "Aceptar",
                    disabled: files.length === 0,
                    secondary: true,
                    onTouchTap: _this.handleClose
                })
            ];

            return _react2.default.createElement(
                _Dialog2.default,
                {
                    title:
                        "Adjuntar " +
                        (files.length > 0 ? files.length : "") +
                        " archivos",
                    actions: actions,
                    modal: false,
                    autoScrollBodyContent: true,
                    contentStyle: { width: "700px" },
                    open: open,
                    onRequestClose: _this.handleClose
                },
                _react2.default.createElement(
                    _List.List,
                    null,
                    files.map(function(file, index) {
                        return _react2.default.createElement(
                            _FileItem2.default,
                            {
                                key: "list-item-file-" + index,
                                name: file.name,
                                type: file.type,
                                size: file.size,
                                onRemove: function onRemove() {
                                    _this.handleRemoveIcon(index);
                                }
                            }
                        );
                    })
                )
            );
        };

        _this.handleOnChangeInput = _this.handleOnChangeInput.bind(_this);
        _this.handleClose = _this.handleClose.bind(_this);

        _this.state = {
            files: []
        };
        return _this;
    }

    return FileManager;
})(_react.Component)),
(_class.propTypes = {
    open: _propTypes2.default.bool,
    onChange: _propTypes2.default.func,
    onClose: _propTypes2.default.func.isRequired
}),
(_class.defaultProps = {
    open: false
}),
_temp);
exports.default = FileManager;
