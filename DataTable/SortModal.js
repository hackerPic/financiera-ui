"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = require("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _Dialog = require("material-ui/Dialog");

var _Dialog2 = _interopRequireDefault(_Dialog);

var _FlatButton = require("material-ui/FlatButton");

var _FlatButton2 = _interopRequireDefault(_FlatButton);

var _List = require("material-ui/List");

var _Divider = require("material-ui/Divider");

var _Divider2 = _interopRequireDefault(_Divider);

var _Checkbox = require("material-ui/Checkbox");

var _Checkbox2 = _interopRequireDefault(_Checkbox);

var _Toggle = require("material-ui/Toggle");

var _Toggle2 = _interopRequireDefault(_Toggle);

var _formats = require("../util/formats");

function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : { default: obj };
}

var SortModal = function SortModal(props) {
    var open = props.open,
        sortName = props.sortName,
        sortType = props.sortType,
        onChangeSortType = props.onChangeSortType,
        onClose = props.onClose,
        onFilter = props.onFilter,
        distinct = props.distinct,
        labelOrder = props.labelOrder,
        onCheckItem = props.onCheckItem;

    var getOptionsGroup = function getOptionsGroup() {
        var options = distinct.columns[sortName];

        return options.collection.map(function(item, index) {
            if (typeof item === "undefined" || item === null) {
                return;
            }

            var label = item;
            if (options.format !== "undefined" && options.format.length > 0) {
                label = (0, _formats.getFormat)({
                    type: options.format,
                    value: item
                });
            }
            var checked =
                options.selected.length > 0 &&
                options.selected.findIndex(function(i) {
                    return i === item;
                }) > -1;

            return _react2.default.createElement(_List.ListItem, {
                key: "distinct-option-" + index,
                primaryText: label,
                leftCheckbox: _react2.default.createElement(
                    _Checkbox2.default,
                    {
                        checked: checked,
                        onCheck: function onCheck(event, value) {
                            onCheckItem(item, value, sortName);
                        }
                    }
                )
            });
        });
    };

    var actions = [
        _react2.default.createElement(_FlatButton2.default, {
            label: "Cancelar",
            onClick: onClose
        }),
        _react2.default.createElement(_FlatButton2.default, {
            label: "Filtrar",
            onClick: onFilter
        })
    ];

    return _react2.default.createElement(
        _Dialog2.default,
        {
            title: "Mostrar solo filas con valores de " + labelOrder + ":",
            actions: actions,
            modal: false,
            autoScrollBodyContent: true,
            contentStyle: { width: "450px" },
            open: open
        },
        open &&
            _react2.default.createElement(
                _List.List,
                null,
                _react2.default.createElement(_List.ListItem, {
                    primaryText: "\xBFOrden ascendente?",
                    rightToggle: _react2.default.createElement(
                        _Toggle2.default,
                        {
                            toggled: sortType === "asc",
                            onToggle: onChangeSortType
                        }
                    )
                }),
                _react2.default.createElement(_Divider2.default, null),
                getOptionsGroup(distinct)
            )
    );
};

SortModal.propTypes = {
    labelOrder: _propTypes2.default.string,
    sortType: _propTypes2.default.string,
    open: _propTypes2.default.bool,
    onChangeSortType: _propTypes2.default.func,
    onCheckItem: _propTypes2.default.func,
    onClose: _propTypes2.default.func,
    onFilter: _propTypes2.default.func,
    distinct: _propTypes2.default.object
};

exports.default = SortModal;
