"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = undefined;

var _classCallCheck2 = require("babel-runtime/helpers/classCallCheck");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _possibleConstructorReturn2 = require("babel-runtime/helpers/possibleConstructorReturn");

var _possibleConstructorReturn3 = _interopRequireDefault(
    _possibleConstructorReturn2
);

var _inherits2 = require("babel-runtime/helpers/inherits");

var _inherits3 = _interopRequireDefault(_inherits2);

var _class, _temp;

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = require("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _elapsedTimeUtil = require("./elapsedTimeUtil");

function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : { default: obj };
}

var ElapsedTime = ((_temp = _class = (function(_Component) {
    (0, _inherits3.default)(ElapsedTime, _Component);

    function ElapsedTime(props, context) {
        (0, _classCallCheck3.default)(this, ElapsedTime);

        var _this = (0, _possibleConstructorReturn3.default)(
            this,
            (ElapsedTime.__proto__ || Object.getPrototypeOf(ElapsedTime)).call(
                this,
                props,
                context
            )
        );

        _this.componentDidMount = function() {
            var _this$props = _this.props,
                onlyDuration = _this$props.onlyDuration,
                addedTime = _this$props.addedTime;

            if (onlyDuration) {
                _this.setState((0, _elapsedTimeUtil.getDuration)(addedTime));
            } else {
                _this.start();
            }
        };

        _this.componentWillReceiveProps = function(nextProps) {
            if (_this.props.stop !== nextProps.stop && !nextProps.stop) {
                _this.start();
            }
            if (!_this.props.onlyDuration && nextProps.onlyDuration) {
                _this.stop();
                _this.setState(
                    (0, _elapsedTimeUtil.getDuration)(nextProps.addedTime)
                );
            }
        };

        _this.componentWillUnmount = function() {
            _this.stop();
        };

        _this.start = function() {
            var interval = _this.props.interval;

            _this.timer = setInterval(_this.counting, interval);
        };

        _this.stop = function() {
            clearInterval(_this.timer);
        };

        _this.counting = function() {
            var _this$props2 = _this.props,
                dateInitial = _this$props2.dateInitial,
                dateFinal = _this$props2.dateFinal,
                addedTime = _this$props2.addedTime;

            _this.setState(
                (0, _elapsedTimeUtil.getDifferenceTime)({
                    dateFinal: dateFinal,
                    dateInitial: dateInitial,
                    addedTime: addedTime
                })
            );
        };

        _this.render = function() {
            var _this$state = _this.state,
                years = _this$state.years,
                months = _this$state.months,
                days = _this$state.days,
                hours = _this$state.hours,
                minutes = _this$state.minutes,
                seconds = _this$state.seconds;
            var title = _this.props.title;

            return _react2.default.createElement(
                "div",
                { className: "mn-header-info-column" },
                _react2.default.createElement(
                    "div",
                    { className: "label" },
                    title
                ),
                _react2.default.createElement(
                    "div",
                    { className: "value" },
                    _react2.default.createElement(
                        "span",
                        null,
                        years > 0 && years + " a\xF1os, ",
                        months > 0 && months + " meses y",
                        days > 0 && days + " d\xEDas"
                    ),
                    days > 0 && _react2.default.createElement("br", null),
                    _react2.default.createElement(
                        "span",
                        null,
                        hours +
                            " horas, " +
                            minutes +
                            " min. y " +
                            seconds +
                            " seg."
                    )
                )
            );
        };

        _this.counting = _this.counting.bind(_this);

        _this.state = {
            years: 0,
            months: 0,
            days: 0,
            hours: 0,
            minutes: 0,
            seconds: 0
        };
        return _this;
    }

    return ElapsedTime;
})(_react.Component)),
(_class.propTypes = {
    dateInitial: _propTypes2.default.number,
    dateFinal: _propTypes2.default.number,
    title: _propTypes2.default.string,
    stop: _propTypes2.default.bool,
    onlyDuration: _propTypes2.default.bool,
    interval: _propTypes2.default.number,
    addedTime: _propTypes2.default.number
}),
(_class.defaultProps = {
    title: "TIEMPO TRANSCURRIDO",
    interval: 1000,
    addedTime: 0,
    stop: false,
    onlyDuration: false
}),
_temp);
exports.default = ElapsedTime;
