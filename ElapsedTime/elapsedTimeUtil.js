"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.getDuration = exports.getDifferenceTime = undefined;

var _moment = require("moment");

var _moment2 = _interopRequireDefault(_moment);

function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : { default: obj };
}

var getDifferenceTime = (exports.getDifferenceTime = function getDifferenceTime(
    data
) {
    var dateFinal = data.dateFinal,
        dateInitial = data.dateInitial,
        addedTime = data.addedTime;

    dateInitial = _moment2.default.unix(parseInt(dateInitial / 1000, 10));

    if (typeof dateFinal !== "undefined") {
        dateFinal = _moment2.default.unix(parseInt(dateFinal / 1000, 10));
    } else {
        dateFinal = (0, _moment2.default)();
    }

    if (addedTime > 0) {
        dateFinal = dateFinal.add(addedTime, "milliseconds");
    }

    var diff = _moment2.default.duration(dateFinal.diff(dateInitial)),
        years = diff.years(),
        months = diff.months(),
        days = diff.days(),
        hours = diff.hours(),
        minutes = diff.minutes(),
        seconds = diff.seconds();

    return {
        years: years,
        months: months,
        days: days,
        hours: hours,
        minutes: minutes,
        seconds: seconds
    };
});
var getDuration = (exports.getDuration = function getDuration(milliseconds) {
    var tempTime = _moment2.default.duration(milliseconds, "milliseconds");

    var years = tempTime.years();
    var months = tempTime.months();
    var days = tempTime.days();
    var hours = tempTime.hours();
    var minutes = tempTime.minutes();
    var seconds = tempTime.seconds();

    return {
        years: years,
        months: months,
        days: days,
        hours: hours,
        minutes: minutes,
        seconds: seconds
    };
});
